﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }

        public AppDbContext()
        {
            if (this.Database.EnsureCreated())
                Seed();
        }

        private void Seed()
        {
            Person p1 = new Person() {Name="Kenneth", Age = 41};
            Person p2 = new Person() {Name="Carmen", Age = 23};
            Person p3 = new Person() {Name="Stijn", Age = 27};

            People.Add(p1);
            People.Add(p2);
            People.Add(p3);

            this.SaveChanges();

            this.ChangeTracker.Clear();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=myappdb.sqlite");
                optionsBuilder.LogTo(msg => System.Diagnostics.Debug.WriteLine(msg), LogLevel.Information);
            }
        }
    }
}