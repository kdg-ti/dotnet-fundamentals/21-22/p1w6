﻿using System.Collections.Generic;
using Domain;

namespace DAL
{
    public interface IRepository
    {
        IEnumerable<Person> ReadAllPeople(string name = null, int? age = null);
    }
}