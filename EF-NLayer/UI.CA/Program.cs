﻿using System;
using System.Linq;
using BL;
using DAL;

namespace UI.CA
{
    class Program
    {
        #region entry-point application + DI
        static void Main(string[] args)
        {
            AppDbContext ctx = new AppDbContext();
            IRepository repo = new Repository(ctx);
            IManager mgr = new Manager(repo);
            Program program = new Program(mgr);
            program.Run();
        }
        #endregion

        private readonly IManager _mgr;
        public Program(IManager mgr)
        {
            _mgr = mgr;
        }

        void Run()
        {
            var people1 = _mgr.GetAllPeople("e", 15);

            foreach (var person in people1)
            {
                Console.WriteLine(person.Name);
            }
            
            var people2 = _mgr.GetAllPeople("e");
            people2 = people2.Where(p => p.Age >= 30);

            foreach (var person in people2)
            {
                Console.WriteLine(person.Name);
            }

        }
    }
}